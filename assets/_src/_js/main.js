/* eslint-disable one-var */
/* eslint-disable no-undef */
/* eslint-disable no-tabs */
// import smoothscroll from 'smoothscroll-polyfill';
// import fancybox from '@fancyapps/fancybox';


/* Variables */
/* ----------------------------------------- */
	const expandWidth = 768;
/* ----------------------------------------- Variables */

/* After Expand */
/* ----------------------------------------- */
	// if ($(window).width() < expandWidth) {}
/* ----------------------------------------- After Expand */

/* Default Scripts */
/* ----------------------------------------- */
	// Mascara de DDD e 9 dígitos para telefones
	// let SPMaskBehavior = function(val) {
	// 	return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	// 	}, spOptions = {onKeyPress: function(val, e, field, options) {
	// 	field.mask(SPMaskBehavior.apply({}, ...e), options);
	// }};
	// $('.mask-phone, input[type="tel"], .wpcf7-tel').mask(SPMaskBehavior, spOptions);
	// SmoothScroll
	// smoothscroll.polyfill();
	// window.scroll({ top: 0, left: 0, behavior: 'smooth' });
	// document.querySelector('header').scrollIntoView({ behavior: 'smooth' });
/* ----------------------------------------- Default Scripts */

/* Mobile Menu */
/* ----------------------------------------- */
	const tlMenu = new TimelineMax({paused: true, onComplete: function() {
		console.log('Foi');
	}});
	tlMenu.set('.navbar--menu li', {y: 40, autoAlpha: 0});
	tlMenu.to('.navbar--container', .4, {autoAlpha: 1, ease: Power0.easeNone});
	tlMenu.staggerTo('.navbar--menu li', .55, {y: 0, autoAlpha: 1, ease: Power4.easeOut}, .25);

	if ($(window).width() <= expandWidth) {
		$('.js-toggle-menu').on('click', function() {
			if (tlMenu.progress() == 1) {
				tlMenu.timeScale(1.5).reverse();
			} else {
				tlMenu.timeScale(1).play();
			}
		});
	}
/* ----------------------------------------- Mobile Menu */
