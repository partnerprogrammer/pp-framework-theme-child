<?php

// Scripts that can be shared between websites
include('inc/default-functions.php');

// Add Hooks (filters & actions) for this website
include('inc/custom-hooks.php');
