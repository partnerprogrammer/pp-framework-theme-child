<?php



add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function my_theme_enqueue_styles() {     
    
    $path_js = get_stylesheet_directory_uri() . '/assets/js/';
 	$path_css = get_stylesheet_directory_uri() . '/assets/css/';
            		
	wp_deregister_script( 'wp-embed' );
	 	
	// wp_localize_script( 'jquery', 'siteVars', [] );						
    // wp_enqueue_script('jquery-mask', '//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js', ['jquery'], false, true);

	/* Choices JS */
	/* ----------------------------------------- */
		// wp_enqueue_script('choices-js', '//cdn.jsdelivr.net/npm/choices.js@4.1.0/public/assets/scripts/choices.min.js', ['jquery'], false, true);
		// wp_enqueue_style( 'choices-css', '//cdn.jsdelivr.net/npm/choices.js@4.1.0/public/assets/styles/choices.min.css');
	/* ----------------------------------------- Choices JS */
		
	/* Caleran JS */
	/* ----------------------------------------- */
		// wp_enqueue_script('moment-js', '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js', ['jquery'], false , true);
		// wp_enqueue_script('momentjs-ptbr', '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/locale/pt-br.js', ['moment-js'], '2.20.1', true );
		// wp_enqueue_script('caleran-js', $path_js.'caleran.min.js', ['jquery', 'moment-js'], false , true);
		// wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	/* ----------------------------------------- Caleran JS */
	
	/* Scroll Magic */
	/* ----------------------------------------- */
		// wp_enqueue_script('scrollmagic', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js', [], false, true);				
		// wp_enqueue_script('scrollmagic--debug', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', ['scrollmagic'], false, true);
	/* ----------------------------------------- Scroll Magic */
	

	/* GSAP */
	/* ----------------------------------------- */
		wp_enqueue_script('gsap', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js', ['jquery'], false, true);		
		// wp_enqueue_script('gsap-css-plugin', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSPlugin.min.js', [], false, true);				
		// wp_enqueue_script('gsap-css-rule-plugin', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSRulePlugin.min.js', [], false, true);				
		// wp_enqueue_script('scrollmagic--gsap', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js', [], false, true);		
	/* ----------------------------------------- GSAP */
	
	
	/* Custons */
	/* ----------------------------------------- */
		// wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=');
		wp_enqueue_style( 'bootstrap', $path_css . 'bootstrap/bootstrap.css');
	  	wp_enqueue_style( 'main', $path_css . 'main.css');
		
	/* ----------------------------------------- Custons */	
}
